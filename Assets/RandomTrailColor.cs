using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RandomTrailColor : MonoBehaviour
{
    public List<Color> colors = new List<Color>();

    private void Start()
    {
        var gradient = new Gradient();

        // Blend color from red at 0% to blue at 100%
        var colorsk = new GradientColorKey[1];
        colorsk[0] = new GradientColorKey(colors[Random.Range(0, colors.Count)], 0.0f);

        // Blend alpha from opaque at 0% to transparent at 100%
        var alphas = new GradientAlphaKey[2];
        alphas[0] = new GradientAlphaKey(1.0f, 0.0f);
        alphas[1] = new GradientAlphaKey(0.0f, 1.0f);

        gradient.SetKeys(colorsk, alphas);

        GetComponent<TrailRenderer>().colorGradient = gradient;
    }
}
