using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CMF
{
    //This character movement input class is an example of how to get input from a keyboard to control the character;
    public class CharacterBotInput : CharacterInput
    {


        public string horizontalInputAxis = "Horizontal";
        public string verticalInputAxis = "Vertical";
        public KeyCode jumpKey = KeyCode.Space;

        private float randomHorizontalInput;

        //If this is enabled, Unity's internal input smoothing is bypassed;
        public bool useRawInput = true;

        private void Start()
        {
            StartCoroutine(SelectRandomInput());
        }

        public override float GetHorizontalMovementInput()
        {
            return randomHorizontalInput;
        }

        public override float GetVerticalMovementInput()
        {
            return 1;
        }

        public override bool IsJumpKeyPressed()
        {
            return false;
        }

        private IEnumerator SelectRandomInput()
        {
            while (true)
            {
                yield return new WaitForSeconds(3);
                randomHorizontalInput = Random.Range(-1f, 1f);
            }
        }
    }
}
