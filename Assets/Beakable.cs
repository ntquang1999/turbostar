using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beakable : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject, 0.4f);
    }
}
