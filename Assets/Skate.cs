using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Skate : MonoBehaviour
{
    [SerializeField] private Transform raycastOrigin;
    [SerializeField] private Transform raycastTarget;
    [SerializeField] private GameObject debugSphere;
    [SerializeField] private LayerMask groundLayer;

    private Rigidbody rb;

    private Vector3 gravity;

    private Vector3 velocity;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        velocity = Vector3.zero;

        if (GroundCheck())
        {
            if (Physics.Raycast(raycastOrigin.position, raycastTarget.position - raycastOrigin.position, out RaycastHit hit, float.MaxValue, groundLayer))
            {
                var fwd = Vector3.ProjectOnPlane(transform.forward, hit.normal);
                transform.rotation = Quaternion.LookRotation(fwd, hit.normal);
                gravity = -hit.normal;
                //Debug.DrawRay(hit.point, hit.normal, Color.red);
            }
            else
            {
                print("hit");
            }

            

            if (Input.GetKey(KeyCode.A))
            {
                velocity += -transform.right * 30f;
            }

            if (Input.GetKey(KeyCode.D))
            {
                velocity += transform.right * 30f;
            }

        }
        else
            gravity = Vector3.down;


        ApplyGravity();


        if (Input.GetKey(KeyCode.W))
        {
            velocity += transform.forward * 30f;
        }

        rb.velocity = velocity;
    }

    private void ApplyGravity()
    {
        velocity += gravity * 7f;
            
        //Debug.DrawRay(raycastOrigin.position, gravity);
    }


    private void OnDrawGizmos()
    {
        //Gizmos.DrawSphere(transform.position, 0.5f);
    }

    private bool GroundCheck()
    {
        
        Collider[] possibleGround = Physics.OverlapSphere(transform.position, 0.5f, groundLayer);
        if (possibleGround.Length > 0)
        {
            return true;
        }
        else
        { return false; }
    }
}
