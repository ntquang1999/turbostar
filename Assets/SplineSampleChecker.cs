using Dreamteck.Splines;
using UnityEngine;

public class SplineSampleChecker : MonoBehaviour
{
    [SerializeField] private SplineComputer spline;
    [SerializeField] private GameObject debugSphereRed;
    [SerializeField] private GameObject debugSphereGreen;

    private int curentPointIndex = 0;
    private int nextPointIndex = 1;
    private SampleCollection sampleCollection = new SampleCollection();
    private GameObject debugRed;
    private GameObject debugGreen;

    private void Start()
    {
        spline.GetSamples(sampleCollection);
        //print(sampleCollection.length);
        //debugRed = Instantiate(debugSphereRed, sampleCollection.GetSample(curentPointIndex).position, Quaternion.identity);
        //debugGreen = Instantiate(debugSphereGreen, sampleCollection.GetSample(nextPointIndex).position, Quaternion.identity);
        
    }

    private void Update()
    {
        if(!IsOppsiteDirection(curentPointIndex, nextPointIndex))
        {
            for(int i = curentPointIndex + 1; i < sampleCollection.length - 1; i++)
            {
                if(IsOppsiteDirection(curentPointIndex, i))
                {
                    curentPointIndex = i - 1;
                    nextPointIndex = i;
                    break;
                }
            }
        }
        //debugRed.transform.position = sampleCollection.GetSample(curentPointIndex).position;
        //debugGreen.transform.position = sampleCollection.GetSample(nextPointIndex).position;
    }

    private bool IsOppsiteDirection(int index1, int index2)
    {
        Vector3 playerPos = GetClosestPointOnLine(transform.position, sampleCollection.GetSample(index1).position, sampleCollection.GetSample(index2).position);

        Vector3 vector1 = playerPos - sampleCollection.GetSample(index1).position;
        Vector3 vector2 = playerPos - sampleCollection.GetSample(index2).position;

        float dotProduct = Vector3.Dot(vector1, vector2);

        if (dotProduct < 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private Vector3 GetClosestPointOnLine(Vector3 point, Vector3 line_start, Vector3 line_end)
    {
        return line_start + Vector3.Project(point - line_start, line_end - line_start);
    }


    public Vector3 GetDirectionVector()
    {
        return sampleCollection.GetSample(curentPointIndex).forward;
    }

    public Vector3 GetUpVector()
    {
        return sampleCollection.GetSample(nextPointIndex).up;
    }

    public SplineSample GetCurrentSplineSample()
    {
        return sampleCollection.GetSample(curentPointIndex);
    }
}
