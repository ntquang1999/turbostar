using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomHatColor : MonoBehaviour
{

    public List<Color> colors = new List<Color>();

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material.color = colors[Random.Range(0, colors.Count)];
    }

}
