Shader "Surfaces/Simple" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Main Color", Vector) = (1,1,1,1)
		[Header(Lighting and Shading)] [Toggle(LIGHTING_ENABLED)] _LightingToggle ("Lighting", Float) = 0
		[Header(Transparency)] [Toggle(TRANSPARENCY)] _Transparency ("Transparency", Float) = 0
		[Toggle(USE_VERTEX_ALPHA)] _UseVertexAlpha ("UseVertexAlpha", Float) = 0
		_Visibility ("Visibility", Range(0, 1)) = 1
		[Toggle(CUTOUT_TRANSPARENCY)] _CutoutTransparency ("Cutout Transparency", Float) = 0
		_CutOff ("Cutout Amount", Range(0, 1)) = 1
		[Toggle(DISTANCE_VISIBILITY)] _DistanceTransparency ("Distance Visibility", Float) = 0
		_DistanceVisibility ("Visibility Distance", Float) = 8
		[Header(Effects)] [Toggle(RIM_LIGHTING)] _RimLighting ("Rim Lighting", Float) = 0
		_RimColor ("Rim Color", Vector) = (1,1,1,1)
		_RimPower ("Rim Power", Range(0.01, 10)) = 3
		[Toggle(GRAYSCALE)] _GrayscaleToggle ("Grayscale", Float) = 0
		_Grayscale ("Grayscale Amount", Range(0, 1)) = 0
		[Toggle(FOG)] _FogToggle ("Fog", Float) = 1
		[Header(Geometry)] [Toggle(CURVED_WORLD)] _CurvedWorld ("Curved World", Float) = 1
		[Header(Other)] [Enum(Two Sided,0, Back,1, Front,2)] _CullMode ("Culling Mode", Float) = 0
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType"="Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		struct Input
		{
			float2 uv_MainTex;
		};
		
		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
}