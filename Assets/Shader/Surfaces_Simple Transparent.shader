Shader "Surfaces/Simple Transparent" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Main Color", Vector) = (1,1,1,1)
		[Header(Geometry)] [Toggle(CURVED_WORLD)] _CurvedWorld ("Curved World", Float) = 1
		[Header(Other)] [Enum(Two Sided,0, Back,1, Front,2)] _CullMode ("Culling Mode", Float) = 0
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType"="Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		struct Input
		{
			float2 uv_MainTex;
		};
		
		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
}