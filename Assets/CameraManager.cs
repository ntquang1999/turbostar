using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private SplineSampleChecker sampleChecker;
    [SerializeField] private Transform followTarget;
    Vector3 currentVelo = Vector3.zero;
    private void Update()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, sampleChecker.GetCurrentSplineSample().rotation, 5 * Time.deltaTime);
        //transform.position = Vector3.Lerp(transform.position, followTarget.position, 5 * Time.deltaTime);


        transform.position = Vector3.SmoothDamp(transform.position, followTarget.position, ref currentVelo, .1f);
    }
}
