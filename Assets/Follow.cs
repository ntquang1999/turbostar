using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform target;

    public Vector3 cachedPos;
    public Quaternion cachedRot;

    private void Update()
    {
        transform.position = cachedPos;
        transform.rotation = cachedRot;

        transform.position = Vector3.Lerp(transform.position, target.position, 2 * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, 2 * Time.deltaTime);

        cachedPos = transform.position;
        cachedRot = transform.rotation;
    }
}
