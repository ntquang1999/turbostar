using CMF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;

public class Gravity : MonoBehaviour
{
    public bool isBot;
    public Mover mover;
    public AdvancedWalkerController walker;
    [SerializeField] private Transform raycastOrigin;
    [SerializeField] private Transform raycastTarget;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Transform camTarget;
    [SerializeField] private Transform camTargetLow;
    [SerializeField] private Transform camTargetNormal;
    private Quaternion targetRot;

    private bool updateRotation = true;

    public bool pauseCheck = false;

    private void Awake()
    {
        Application.targetFrameRate = 200;
    }

    private void Start()
    {
        Invoke("Break", 3);
    }

    private void Break()
    {
        pauseCheck = true;
    }
    private void Update()
    {
        if(!isBot)
        {
            if(GetSideValue(transform, GetComponent<SplineSampleChecker>().GetCurrentSplineSample()) > 20)
            {
                camTarget.position = Vector3.Lerp(camTarget.position, camTargetLow.position, 10 * Time.deltaTime);
            }
            else
            {
                camTarget.position = Vector3.Lerp(camTarget.position, camTargetNormal.position, 10 * Time.deltaTime);
            }

            //print(GetHeightValue());
        }

        if (!updateRotation)
            return;

        if(mover.IsGrounded())
        {
            if (Physics.Raycast(raycastOrigin.position, raycastTarget.position - raycastOrigin.position, out RaycastHit hit, float.MaxValue, groundLayer))
            {
                walker.animator.SetBool("IsGrounded", true);
                walker.stickmanAnimator.SetBool("DoingTrick", walker.doingTrick);
                var fwd = Vector3.ProjectOnPlane(GetComponent<SplineSampleChecker>().GetDirectionVector(), hit.normal);
                targetRot = Quaternion.LookRotation(fwd, hit.normal);

                if (hit.collider.CompareTag("Roof"))
                {
                    if (!walker.doingTrick)
                    {
                        if(!IsOnLeftSide(transform, GetComponent<SplineSampleChecker>().GetCurrentSplineSample()))
                            StartCoroutine(walker.DoTrickLeft());
                        else
                            StartCoroutine(walker.DoTrickRight());
                    }
                }
            }
        }
        else
        {
            if(!Physics.Raycast(raycastOrigin.position, raycastTarget.position - raycastOrigin.position, out RaycastHit hit, 1f, groundLayer))
            {
                var fwd = Vector3.ProjectOnPlane(GetComponent<SplineSampleChecker>().GetDirectionVector(), GetComponent<SplineSampleChecker>().GetUpVector());
                targetRot = Quaternion.LookRotation(fwd, GetComponent<SplineSampleChecker>().GetUpVector());
                walker.animator.SetTrigger("FlipTrick");
                walker.stickmanAnimator.SetBool("DoingTrick", true);
                walker.animator.SetBool("IsGrounded", false);
            }
        }

        
    }

    //private IEnumerator DoTrickLeft()
    //{
    //    doingTrick = true; 
    //    walker.forceLeftInput = true;
    //    walker.ToggleHozInput();
    //    animator.SetTrigger("Left");
    //    yield return new WaitForSeconds(.4f);
    //    walker.forceRightInput = true;
    //    walker.forceLeftInput = false;
    //    yield return new WaitForSeconds(.65f);
        
    //    walker.forceRightInput = false;
    //    walker.ToggleHozInput();
    //    doingTrick = false;
    //}

    //private IEnumerator DoTrickRight()
    //{
    //    doingTrick = true;
    //    walker.forceRightInput = true;
    //    walker.ToggleHozInput();
    //    animator.SetTrigger("Left");
    //    yield return new WaitForSeconds(0.1f);
    //    walker.forceRightInput = false;
    //    walker.hozLerpSpeed = 3f;
    //    walker.forceNoInput = true;
    //    yield return new WaitForSeconds(0.6f);
    //    walker.forceNoInput = false;
    //    walker.forceLeftInput = true;
    //    yield return new WaitForSeconds(0.4f);
    //    walker.hozLerpSpeed = 1f;
    //    walker.forceLeftInput = false;
    //    walker.ToggleHozInput();
    //    doingTrick = false;
    //}

    private void FixedUpdate()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, 40 * Time.fixedDeltaTime);
        //print(walker.GetVelocity().magnitude);
    }

    public bool CheckForGround()
    {
        if (mover.IsGrounded())
        {
            if (Physics.Raycast(raycastOrigin.position, raycastTarget.position - raycastOrigin.position, out RaycastHit hit, float.MaxValue, groundLayer))
            {
                if (!hit.collider.CompareTag("Roof"))
                {
                    return true;
                }
            }
        }
        
        return false;
    }

    private bool IsOnLeftSide(Transform testPoint, SplineSample referencePoint)
    {
        Vector3 referenceForward = referencePoint.forward;
        Vector3 referenceUp = referencePoint.up;
        Vector3 referenceToTest = testPoint.position - referencePoint.position;

        Vector3 crossProduct = Vector3.Cross(referenceForward, referenceUp);
        float dotProduct = Vector3.Dot(crossProduct, referenceToTest);
        
        //if(!isBot)
        //    print(dotProduct);

        if (dotProduct > 0)
        {
            return false;
        }
        else if (dotProduct < 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public float GetSideValue(Transform testPoint, SplineSample referencePoint)
    {
        Vector3 referenceForward = referencePoint.forward;
        Vector3 referenceUp = referencePoint.up;
        Vector3 referenceToTest = testPoint.position - referencePoint.position;

        Vector3 crossProduct = Vector3.Cross(referenceForward, referenceUp);
        float dotProduct = Vector3.Dot(crossProduct, referenceToTest);

        return Mathf.Abs(dotProduct);
    }

    public float GetHeightValue(Transform testPoint, SplineSample referencePoint)
    {
        Vector3 referenceForward = referencePoint.forward;
        Vector3 referenceUp = referencePoint.right;
        Vector3 referenceToTest = testPoint.position - referencePoint.position;

        Vector3 crossProduct = Vector3.Cross(referenceForward, referenceUp);
        float dotProduct = Vector3.Dot(crossProduct, referenceToTest);

        return Mathf.Abs(dotProduct);
    }

    public float GetHeightValue()
    {
        return GetHeightValue(transform, GetComponent<SplineSampleChecker>().GetCurrentSplineSample());
    }

    public float GetSideValue()
    {
        return GetSideValue(transform, GetComponent<SplineSampleChecker>().GetCurrentSplineSample());
    }
}
