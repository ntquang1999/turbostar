using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [ExecuteInEditMode]
public class SplineBuilder : MonoBehaviour
{
    private SplineComputer spline;

    [SerializeField]
    private Transform level;

    public bool Rebuild;

    private void Update()
    {
        if(!Rebuild) return;
        Rebuild = false;
        spline = GetComponent<SplineComputer>();
        
        foreach(Locator locator in level.GetComponentsInChildren<Locator>())
        {
            SplinePoint point = new SplinePoint();
            point.position = locator.transform.position;
            point.normal = locator.transform.up;
            point.tangent = locator.transform.forward;
            spline.SetPoint(spline.pointCount, point);
        }
    }
}
