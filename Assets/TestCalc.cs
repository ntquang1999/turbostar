using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCalc : MonoBehaviour
{
    public Transform referencePoint;
    public Transform testPoint;

    private void Update()
    {
        Vector3 referenceForward = referencePoint.forward;
        Vector3 referenceUp = referencePoint.up;
        Vector3 referenceToTest = testPoint.position - referencePoint.position;

        Vector3 crossProduct = Vector3.Cross(referenceForward, referenceUp);
        float dotProduct = Vector3.Dot(crossProduct, referenceToTest);

        if (dotProduct > 0)
        {
            Debug.Log("R");
        }
        else if (dotProduct < 0)
        {
            Debug.Log("L");
        }
        else
        {
            Debug.Log("M");
        }
    }
}
