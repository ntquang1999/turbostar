using CMF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This character movement input class is an example of how to get input from a keyboard to control the character;
public class CharacterTouchInput : CharacterInput
{


    public string horizontalInputAxis = "Horizontal";
    public string verticalInputAxis = "Vertical";
    public KeyCode jumpKey = KeyCode.Space;

    //If this is enabled, Unity's internal input smoothing is bypassed;
    public bool useRawInput = true;

    private bool isTouching;

    private Vector2 touchPos;

    private float cachedDelta;

    private float untouchThreshold = 0.3f;

    public override float GetHorizontalMovementInput()
    {
        if (!inputEnabled)
            return 0f;

        if(Input.touchCount > 0)
        {
            if(!isTouching)
            {
                isTouching = true;
                touchPos = Input.GetTouch(0).position;
            }

            if(isTouching)
            {
                float touchDelta = Input.GetTouch(0).position.x - touchPos.x;

                if(touchDelta > 0f)
                {
                    if (touchDelta - cachedDelta < -0.1f)
                    {
                        touchPos = Input.GetTouch(0).position;
                        return touchDelta - cachedDelta;
                    }
                }

                if(touchDelta < 0f)
                {
                    if (touchDelta - cachedDelta > 0.1f)
                    {
                        touchPos = Input.GetTouch(0).position;
                        return cachedDelta - touchDelta;
                    }
                }

                cachedDelta = touchDelta;

                float inputValue = touchDelta / (Screen.width / 2f);
                float deltaRate = Mathf.Abs(inputValue);

                if(deltaRate < 0.1f)
                {
                    inputValue = 0f;
                }
                else if(deltaRate < 0.5f)
                {
                    inputValue /= 1.3f;
                }
                else
                {

                }

                inputValue = Mathf.Clamp(inputValue, -1f, 1f);

                return inputValue;
            }
            return 0f;
        }
        else
        {
            untouchThreshold -= Time.fixedDeltaTime;
            if(untouchThreshold < 0f)
                isTouching = false;
            return 0;
        }
    }

    public override float GetVerticalMovementInput()
    {
        return 1f;
    }

    public override bool IsJumpKeyPressed()
    {
        if (!inputEnabled)
            return false;

        return Input.GetKey(jumpKey);
    }
}
